package com.tw.jeevithasalary;

import java.util.Scanner;

public class JeevithaSalary {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int[] workingHours = new int[7];
        int salary = 0, baseHours = 8, sunday = 0, saturday = 6, amountPerHour = 100, extraAmount = 115, amountForBaseHours = 800;
        float fiftyPercent = 0.50F, twentyFivePercent = 0.25F;

        for (int day = sunday; day <= saturday; day++) {
            workingHours[day] = scanner.nextInt();
        }


        for (int day = sunday; day <= saturday; day++) {
            if (day > sunday && day < saturday) {
                salary = getSalary(workingHours, salary, baseHours, amountPerHour, extraAmount, amountForBaseHours, day);
            }
            if (day == sunday) {
                salary = getSalaryForSunday(workingHours, salary, baseHours, amountPerHour, extraAmount, amountForBaseHours, fiftyPercent, day);
            }
            if (day == saturday) {
                salary = getSalaryForSaturday(workingHours, salary, baseHours, amountPerHour, extraAmount, amountForBaseHours, twentyFivePercent, day);
            }

        }
        System.out.println("salary" + salary);
    }

    private static int getSalaryForSaturday(int[] workingHours, int salary, int baseHours, int amountPerHour, int extraAmount, int amountForBaseHours, float twentyFivePercent, int day) {

        if (workingHours[day] <= baseHours)
            salary += (workingHours[day] * amountPerHour) + (twentyFivePercent * (workingHours[day] * amountPerHour));
        if (workingHours[day] > baseHours)
            salary += ((workingHours[day] - baseHours) * extraAmount) + amountForBaseHours + (twentyFivePercent * (workingHours[day] * amountPerHour));
        return salary;
    }

    private static int getSalaryForSunday(int[] workingHours, int salary, int baseHours, int amountPerHour, int extraAmount, int amountForBaseHours, float fiftyPercent, int day) {

        if (workingHours[day] <= baseHours)
            salary += (workingHours[day] * amountPerHour) + (fiftyPercent * (workingHours[day] * amountPerHour));
        if (workingHours[day] > baseHours)
            salary += ((workingHours[day] - baseHours) * extraAmount) + amountForBaseHours + (fiftyPercent * (workingHours[day] * amountPerHour));
        return salary;
    }

    private static int getSalary(int[] workingHours, int salary, int baseHours, int amountPerHour, int extraAmount, int amountForBaseHours, int day) {

        if (workingHours[day] <= baseHours)
            salary += workingHours[day] * amountPerHour;
        if (workingHours[day] > baseHours)
            salary += ((workingHours[day] - baseHours) * extraAmount) + amountForBaseHours;
        return salary;
    }
}

